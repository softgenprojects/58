const { PrismaClient } = require('@prisma/client');
const jwt = require('jsonwebtoken');

const prisma = new PrismaClient();

async function verifyWallet(address, signature) {
  const user = await prisma.user.findUnique({
    include: {
      wallet: true
    },
    where: {
      wallet: {
        address: address
      }
    }
  });

  if (!user) {
    throw new Error('User not found');
  }

  try {
    jwt.verify(signature, process.env.JWT_SECRET);
  } catch (error) {
    throw new Error('Signature verification failed');
  }

  return user;
}

module.exports = { verifyWallet };