require('dotenv').config();
const jwt = require('jsonwebtoken');
const { PrismaClient } = require('@prisma/client');

const prisma = new PrismaClient();

const verifyWallet = async (signature, address) => {
  try {
    const decoded = jwt.verify(signature, process.env.JWT_SECRET);
    const user = await prisma.user.findUnique({
      where: {
        address: address
      }
    });
    if (!user) return false;
    return decoded.address === address;
  } catch (error) {
    console.error('JWT verification error:', error);
    return false;
  }
};

module.exports = { verifyWallet };