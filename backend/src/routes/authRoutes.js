const express = require('express');
const { walletLogin } = require('../controllers/authController');

const router = express.Router();

router.post('/wallet-login', walletLogin);

module.exports = router;