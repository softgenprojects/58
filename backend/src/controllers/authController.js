const asyncHandler = require('express-async-handler');
const authService = require('@services/authService');

const walletLogin = asyncHandler(async (req, res) => {
  const { signature, address } = req.body;
  try {
    const user = await authService.verifyWallet(address, signature);
    res.json({
      message: 'Login successful',
      user,
    });
  } catch (error) {
    res.status(401).json({
      message: 'Login failed',
      error: error.message,
    });
  }
});

module.exports = { walletLogin };