const express = require('express');
const authService = require('../services/authService');

const router = express.Router();

const walletLogin = async (req, res) => {
  const { signature, address } = req.body;
  try {
    const user = await authService.verifyWallet(address, signature);
    res.json(user);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

router.post('/wallet-login', walletLogin);

module.exports = router;