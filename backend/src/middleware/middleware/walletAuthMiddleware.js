require('dotenv').config();
const jwt = require('jsonwebtoken');

const walletAuthMiddleware = (req, res, next) => {
  const { signature } = req.body;
  try {
    const decoded = jwt.verify(signature, process.env.JWT_SECRET);
    req.user = decoded;
    next();
  } catch (error) {
    res.status(401).json({ message: 'Invalid signature.' });
  }
};

module.exports = { walletAuthMiddleware };