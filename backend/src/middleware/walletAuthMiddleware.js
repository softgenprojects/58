const jwt = require('jsonwebtoken');

const walletAuthMiddleware = (req, res, next) => {
  const { signature, address } = req.headers;

  if (!signature || !address) {
    return res.status(401).json({ message: 'Authentication failed: signature and address are required.' });
  }

  try {
    jwt.verify(signature, address);
    req.user = { address };
    next();
  } catch (error) {
    return res.status(401).json({ message: 'Authentication failed: invalid signature.' });
  }
};

module.exports = walletAuthMiddleware;